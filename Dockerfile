FROM node:latest AS builder

# On se met dans /app
WORKDIR /app
# Copie le projet
COPY . ./

# On build le projet
RUN npm install
RUN npm install prom-client>=12.0.0
RUN npm run build

FROM mhart/alpine-node:latest

WORKDIR /app

COPY package.json ./
RUN npm install
COPY --from=builder /app/dist/ /app/dist/
CMD [ "npm", "start" ]
